import React, {Component} from 'react';
import {Platform, StyleSheet, Text, TextInput, View, ActivityIndicator, SafeAreaView, ScrollView, StatusBar} from 'react-native';





export default class App3 extends Component {

    constructor(props) {
      super(props)
      this.state =  {
        isLoading: true,
        dataSource: null,
        date: '',
      }
    }

  
      componentDidMount () {



        var that = this

        var date = new Date().getDate() //Current Date
        var month = new Date().getMonth() + 1 //Current Month
        var year = new Date().getFullYear() //Current Year
        var zero = '0'

        var diaSemana = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado']
        var day = new Date()

        var diaS = diaSemana[day.getDay()]
        

        var mesAno = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
        var month = new Date()

        var mesAtual = mesAno[month.getMonth()]



        if (date < 10) {
          date = zero.concat(date)
        }

    
        that.setState({
          //Setting the value of the date time
          date:
            diaS + ', ' + date + ' de ' + mesAtual + ' de ' + year,
        })
        
        let dia = new Date().getDate()
        let mes = new Date().getMonth() +1
        let endereco = 'https://ws.appbeleza.com.br/Service.php?metodo=buscaHorarios&dia=diaA&mes=mesS&ano=2019&profissional=895154'
        return fetch(endereco.replace("diaA", dia).replace("mesS", mes))
  
        
        .then((response) => response.json() )  
          .then ((responseJson) => {
  
            this.setState({
              isLoading: false,
              dataSource: responseJson.horario, //nome obj json
            })
          })
  
          .catch((error) => {
            console.log(console.error)
  
          })
      }
  
    render() {
  
      if(this.state.isLoading) {
  
        return (
          <View >
            <ActivityIndicator />
            </View>
            
        )
      } else {
        

  
          let buscaHorarios = this.state.dataSource.map((val,key) =>  {
          return <View style={styles.container} key={key} >
                      <Text  style={styles.agendamentos}> {val.Descricao}</Text>
                </View>
  
  
        })
          return (
            
            
        <ScrollView style={styles.welcome}>
              <StatusBar
              backgroundColor="blue"
              barStyle="light-content"/>

          <Text style={styles.welcome}>Agendamentos</Text>
          <Text style={styles.data}>{this.state.date}</Text>


         {buscaHorarios}
  
  
        </ScrollView>
      )
      }
    }
  }



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  welcome: {
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'lightgray',
    paddingVertical: 15,
    // margin: 15,
    backgroundColor: 'black',
  },
  agendamentos: {
    width: '80%',
    height: 55,
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    alignItems: 'center',
    marginBottom: 30,
    color: 'lightgray',
    justifyContent: 'center',
    borderColor: '#f0f0f0',
    borderWidth: 1,
    backgroundColor: '#393939',
    borderRadius: 8,
    overflow: 'hidden'

  },
  linha: {
    flex: 1,
    paddingRight: 15,
    marginTop: 8,
    paddingTop: 13,
    paddingBottom: 13,
    // borderBottomWidth: 0.5,
    // borderColor: 'gray',
    // borderRadius: 3,
    flexDirection: 'row',
    alignItems: 'center',
    color: 'lightgray',
    borderWidth: StyleSheet.hairlineWidth,
    margin: StyleSheet.hairlineWidth,
    // backgroundColor: '#333c4c'
  },
  list:{
    borderRadius: 0.8,
    borderColor: '#f0f0f0'
  },
  data: {
    fontSize: 15,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'lightgray',
    justifyContent: 'flex-start',
    marginBottom: 10,
    backgroundColor: 'black',
  },
  scrollView: {
    // backgroundColor: 'pink',
    //marginVertical: 10,
    marginHorizontal: 3,
    textAlign: 'center',
  },
  item: {
    flex: 1,
    flexDirection: 'column',
    margin: 3,
    borderColor: '#222',
    borderWidth: 3,
    borderRadius: 10,
    borderColor: '#449'
  },


});

